User.create!(user_name:  "Example User",
             name: "Example",
             email: "example@railstutorial.org",
             gender: "男",
             comment: "絵を書くことが小さいころから大好きです。",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)
             
User.create!(user_name:  "Example User1",
             name: "Example",
             email: "example-1@railstutorial.org",
             gender: "男",
             comment: "音大生です。",
             password:              "foobar",
             password_confirmation: "foobar")
             
User.create!(user_name:  "Example User2",
             name: "Example",
             email: "example-2@railstutorial.org",
             gender: "男",
             comment: "数々の有名YouTuberの動画を編集してきました。",
             password:              "foobar",
             password_confirmation: "foobar")
             
User.create!(user_name:  "Example User3",
             name: "Example",
             email: "example-3@railstutorial.org",
             gender: "男",
             comment: "1年前からプログラミングをはじめました。",
             password:              "foobar",
             password_confirmation: "foobar")

User.create!(user_name:  "Example User4",
             name: "Example",
             email: "example-4@railstutorial.org",
             gender: "男",
             comment: "数１０社にアドバイスをしてきた実績があります。",
             password:              "foobar",
             password_confirmation: "foobar")
             
User.create!(user_name:  "Example User5",
             name: "Example",
             email: "example-5@railstutorial.org",
             gender: "男",
             comment: "１０年間アメリカに住んでいた日本人です。",
             password:              "foobar",
             password_confirmation: "foobar")             

User.create!(user_name:  "Example User6",
             name: "Example",
             email: "example-6@railstutorial.org",
             gender: "男",
             comment: "製薬会社で美容品研究をしています。",
             password:              "foobar",
             password_confirmation: "foobar") 
             
User.create!(user_name:  "Example User7",
             name: "Example",
             email: "example-7@railstutorial.org",
             gender: "男",
             comment: "昨年年間収支1000万達成しました！！",
             password:              "foobar",
             password_confirmation: "foobar")

User.create!(user_name:  "Example User8",
             name: "Example",
             email: "example-8@railstutorial.org",
             gender: "男",
             comment: "色々な絵がかけます。",
             password:              "foobar",
             password_confirmation: "foobar")

User.create!(user_name:  "Example User9",
             name: "Example",
             email: "example-9@railstutorial.org",
             gender: "男",
             comment: "歌を教えています。",
             password:              "foobar",
             password_confirmation: "foobar")

40.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+10}@railstutorial.org"
  password = "password"
  gender = "女"
  comment = Faker::Lorem.sentence
  User.create!(user_name:  name,
               email: email,
               comment: comment,
               gender: gender,
               password:              password,
               password_confirmation: password)
end
  
users = User.order(:created_at).take(3)
image_path = File.join(Rails.root, "app/assets/images/rails.png")
5.times do
  content = "example title"
  service = Faker::Lorem.sentence(5)
  price = "1000円/60分"
  level = "全対応"
  category = "その他"
  users.each {|user| user.microposts.create!(content: content, price: price,
      service: service, level: level, category: category, picture: File.new(image_path))}
end

userds = User.find(1)
userms = User.find(2)
usermv = User.find(3)
userit = User.find(4)
userbs = User.find(5)
useren = User.find(6)
userbt = User.find(7)
user1 = User.find(8)
userds2 = User.find(9)
userms2 = User.find(10)
content1 = "優しくスロット教えます。"
content2 = "スロットの勝ち方教えます。初心者歓迎！！"
content3 = "常に収支プラス。負けない！！"
contentds = "絵の書き方教えます。"
contentds1 = "風景の描き方教えます。"
contentms = "ピアノ教えます。"
contentms1 = "正しい歌の歌い方教えます。"
contentmv = "動画の作成方法教えます。"
contentit = "プログラミング教えます。"
contentbs = "マーケティング教えます。"
contenten = "英語教えます。"
contentbt = "正しい洗顔の方法教えます。"
service1 = "基本中の基本から説明します。"
service2 = "下見の方法 または店の選び方"
service3 = "台の選び方、店の選び方、損切りの方法"
service = Faker::Lorem.sentence(5)
level1 = "初級者"
level2 = "中級者"
level3 = "上級者"
categoryds = "デザイン"
categoryms = "音楽"
categorymv = "動画、写真、画像"
categoryit = "IT、プログラミング"
categorybs = "ビジネス"
categoryen = "語学"
categorybt = "美容"
category = "趣味、エンタメ"
price1 = "1000円/60分"
price2 = "2000円/60分"
price3 = "3000円/60分"
image1_path = File.join(Rails.root, "app/assets/images/rem.jpeg")
image2_path = File.join(Rails.root, "app/assets/images/win.png")
image3_path = File.join(Rails.root, "app/assets/images/over5000.png")
imagebt_path = File.join(Rails.root, "app/assets/images/beatiful.png")
imagebs_path = File.join(Rails.root, "app/assets/images/business.png")
imageds_path = File.join(Rails.root, "app/assets/images/design.png")
imageen_path = File.join(Rails.root, "app/assets/images/english.png")
imagemv_path = File.join(Rails.root, "app/assets/images/movie.png")
imagems_path = File.join(Rails.root, "app/assets/images/music.png")
imageit_path = File.join(Rails.root, "app/assets/images/programing.png")
imageds1_path = File.join(Rails.root, "app/assets/images/design2.png")
imagems1_path = File.join(Rails.root, "app/assets/images/music1.png")

user1.microposts.create!(content: content3, price: price3,
    service: service3, level: level3, category: category, picture: File.new(image3_path))
userbs.microposts.create!(content: contentbs, price: price3,
    service: service, level: level3, category: categorybs, picture: File.new(imagebs_path))
userms2.microposts.create!(content: contentms1, price: price2,
    service: service, level: level2, category: categoryms, picture: File.new(imagems1_path))
userds2.microposts.create!(content: contentds1, price: price1,
    service: service, level: level3, category: categoryds, picture: File.new(imageds1_path))
userds.microposts.create!(content: contentds, price: price1,
    service: service, level: level1, category: categoryds, picture: File.new(imageds_path))
userit.microposts.create!(content: contentit, price: price3,
    service: service, level: level3, category: categoryit, picture: File.new(imageit_path))    
useren.microposts.create!(content: contenten, price: price2,
    service: service, level: level2, category: categoryen, picture: File.new(imageen_path))
userms2.microposts.create!(content: contentms1, price: price2,
    service: service, level: level1, category: categoryms, picture: File.new(imagems1_path))
userms.microposts.create!(content: contentms, price: price2,
    service: service, level: level2, category: categoryms, picture: File.new(imagems_path))
userds2.microposts.create!(content: contentds1, price: price1,
    service: service, level: level1, category: categoryds, picture: File.new(imageds1_path))
usermv.microposts.create!(content: contentmv, price: price2,
    service: service, level: level2, category: categorymv, picture: File.new(imagemv_path))
user1.microposts.create!(content: content2, price: price2,
    service: service2, level: level2, category: category, picture: File.new(image2_path))
userds.microposts.create!(content: contentds, price: price2,
    service: service, level: level2, category: categoryds, picture: File.new(imageds_path))
user1.microposts.create!(content: content1, price: price1,
    service: service1, level: level1, category: category, picture: File.new(image1_path))
usermv.microposts.create!(content: contentmv, price: price3,
    service: service, level: level3, category: categorymv, picture: File.new(imagemv_path))
userit.microposts.create!(content: contentit, price: price2,
    service: service, level: level2, category: categoryit, picture: File.new(imageit_path))
userbt.microposts.create!(content: contentbt, price: price1,
    service: service, level: level1, category: categorybt, picture: File.new(imagebt_path))
userbs.microposts.create!(content: contentbs, price: price2,
    service: service, level: level2, category: categorybs, picture: File.new(imagebs_path))
useren.microposts.create!(content: contenten, price: price1,
    service: service, level: level1, category: categoryen, picture: File.new(imageen_path))
userms.microposts.create!(content: contentms, price: price3,
    service: service, level: level3, category: categoryms, picture: File.new(imagems_path))
userbt.microposts.create!(content: contentbt, price: price2,
    service: service, level: level2, category: categorybt, picture: File.new(imagebt_path))    


users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }