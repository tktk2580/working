class AddColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :user_name, :string, null: false, default: ""
    add_column :users, :comment, :string
    add_column :users, :gender, :string
  end
end
