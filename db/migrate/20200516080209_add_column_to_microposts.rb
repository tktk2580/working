class AddColumnToMicroposts < ActiveRecord::Migration[5.1]
  def change
    add_column :microposts, :price, :string
    add_column :microposts, :service, :string
    add_column :microposts, :other, :string
    add_column :microposts, :category, :string
  end
end
