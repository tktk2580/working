class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create]

  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.body == ''
      flash[:danger] = "コメント欄に何も記入されていません。"
      redirect_back(fallback_location: root_path)
    else
      if @comment.save
        flash[:success] = "コメントしました。"
        redirect_back(fallback_location: root_path)
        @micropost.create_notification_comment!(current_user, @comment.id)
      else
        flash[:danger] = "100字以内でコメントしてください。"
        redirect_back(fallback_location: root_path)
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
      flash[:success] = "コメントを削除しました。"
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:comment_content, :micropost_id, :user_id, :body)
  end
end
