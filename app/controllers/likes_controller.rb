class LikesController < ApplicationController
  before_action :logged_in_user

  def create
    @micropost = Micropost.find(params[:micropost_id])
    @micropost.like(current_user)
    flash[:success] = "インストラクターに通知を送りました。"
    redirect_to @micropost
    @micropost.create_notification_like!(current_user)
  end

  def destroy
    @micropost = Like.find(params[:id]).micropost
    @micropost.unlike(current_user)
    flash[:danger] = "レッスンをキャンセルしました。"
    redirect_to @micropost
  end
end
