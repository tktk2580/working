class HomeController < ApplicationController
  def index
    @recently_post = Micropost.take(3)
    @level1 = Micropost.where(level: "初級者").limit(4)
    @level2 = Micropost.where(level: "中級者").limit(4)
    @level3 = Micropost.where(level: "上級者").limit(4)
    @designs = Micropost.where(category: "デザイン").limit(4)
    @musics = Micropost.where(category: "音楽").limit(4)
    @movies = Micropost.where(category: "動画、写真、画像").limit(4)
    @its = Micropost.where(category: "IT、プログラミング").limit(4)
    @businesses = Micropost.where(category: "ビジネス").limit(4)
    @languages = Micropost.where(category: "語学").limit(4)
    @beauties = Micropost.where(category: "美容").limit(4)
    @hobbies = Micropost.where(category: "趣味、エンタメ").limit(4)
    @others = Micropost.where(category: "その他").limit(4)
    if user_signed_in?
      @feed_items = current_user.feed.paginate(page: params[:page], per_page: 8)
    end
  end

  def search
    @microposts = Micropost.search_micropost(params[:search])
  end
end
