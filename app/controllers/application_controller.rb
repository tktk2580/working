class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def logged_in_user
    unless user_signed_in?
      flash[:danger] = "ログインしてください。"
      redirect_to login_url
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :user_name, :comment, :gender])
  end
end
