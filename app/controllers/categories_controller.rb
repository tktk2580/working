class CategoriesController < ApplicationController
  def category
  end

  def design
    @title = "デザイン"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def music
    @title = "音楽"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def movie
    @title = "動画、写真、画像"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def it
    @title = "IT、プログラミング"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def business
    @title = "ビジネス"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def language
    @title = "語学"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def beauty
    @title = "美容"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def hobby
    @title = "趣味、エンタメ"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def other
    @title = "その他"
    @categories = Micropost.where(category: @title)
    render "category"
  end

  def level1
    @title = "初級者"
    @categories = Micropost.where(level: @title)
    render "category"
  end

  def level2
    @title = "中級者"
    @categories = Micropost.where(level: @title)
    render "category"
  end

  def level3
    @title = "上級者"
    @categories = Micropost.where(level: @title)
    render "category"
  end
end
