class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :destroy]
  before_action :correct_user,   only: [:edit, :destroy]

  def new
    @micropost = current_user.microposts.build
  end

  def show
    @micropost = Micropost.find(params[:id])
    @user = @micropost.user
    @microposts = @user.microposts.paginate(page: params[:page], per_page: 6).
      where.not(id: @micropost.id)
    @relativeposts = Micropost.paginate(page: params[:page], per_page: 6).
      where(category: @micropost.category).where.not(user_id: @micropost.user_id)
    @comment = Comment.new
    @comments = @micropost.comments
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "新しいLessonを開講しました！！"
      redirect_to @micropost
    else
      @feed_items = []
      render 'new'
    end
  end

  def edit
    @micropost = Micropost.find(params[:id])
  end

  def update
    @micropost = Micropost.find(params[:id])
    if @micropost.update_attributes(micropost_params)
      flash[:success] = "編集成功！！"
      redirect_to @micropost
    else
      render 'edit'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Lessonを削除しました。"
    redirect_to root_url
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture,
                                      :price, :service, :other, :category, :level)
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end
end
