Rails.application.routes.draw do
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    registrations: 'users/registrations',
    sessions: 'users/sessions',
  }
  root 'home#index'
  get 'search',   to: 'home#search'
  get 'design',   to: 'categories#design'
  get 'music',    to: 'categories#music'
  get 'movie',    to: 'categories#movie'
  get 'it',       to: 'categories#it'
  get 'business', to: 'categories#business'
  get 'language', to: 'categories#language'
  get 'beauty',   to: 'categories#beauty'
  get 'hobby',    to: 'categories#hobby'
  get 'other',    to: 'categories#other'
  get 'level1',    to: 'categories#level1'
  get 'level2',    to: 'categories#level2'
  get 'level3',    to: 'categories#level3'
  devise_scope :user do
    get     '/signup',         to: 'users/registrations#new'
    post    '/signup',         to: 'users/registrations#create'
    get     '/users/edit',     to: 'users/registrations#edit'
    patch   '/users/:id',      to: 'users/registrations#update'
    get     'login',           to: 'users/sessions#new'
    post    '/login',          to: 'users/sessions#create'
    delete  '/logout',         to: 'users/sessions#destroy'
    get     '/password/reset/new', to: 'users/passwords#new'
    post    'user/password',       to: 'users/passwords#create'
    get     'users/password/edit', to: 'users/passwords#edit'
    patch   'users/password', to: 'users/passwords#update'
    get     'profile_edit', to: 'users/registrations#profile_edit', as: 'profile_edit'
    patch   'profile_update', to: 'users/registrations#profile_update', as: 'profile_update'
  end
  resources :users
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts, only: [:new, :show, :create, :edit, :update, :destroy] do
    resources :comments, only: [:create, :destroy]
  end
  resources :relationships, only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
  resources :notifications, only: :index
end
