require 'rails_helper'

RSpec.describe Micropost, type: :model do
  let(:micropost) { FactoryBot.build(:orange) }

  it 'sample data is right' do
    expect(micropost).to be_valid
  end

  it 'user_id not should be nil' do
    micropost.user_id = nil
    expect(micropost).not_to be_valid
  end

  it 'content not should be empty' do
    micropost.content = ' '
    expect(micropost).not_to be_valid
  end

  it 'content is up to 25' do
    micropost.content = 'a' * 26
    expect(micropost).not_to be_valid
  end

  it 'level not should be nil' do
    micropost.level = nil
    expect(micropost).not_to be_valid
  end

  it 'picture not should be nil' do
    micropost.picture = nil
    expect(micropost).not_to be_valid
  end

  it 'price not should be nil' do
    micropost.price = nil
    expect(micropost).not_to be_valid
  end

  it 'service not should be nil' do
    micropost.service = nil
    expect(micropost).not_to be_valid
  end

  it 'service is up to 1000' do
    micropost.service = 'a' * 1001
    expect(micropost).not_to be_valid
  end

  it 'other is up to 1000' do
    micropost.other = 'a' * 1001
    expect(micropost).not_to be_valid
  end

  it 'category not should be nil' do
    micropost.category = nil
    expect(micropost).not_to be_valid
  end
end
