require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:comment) { FactoryBot.build(:kirito) }

  it 'sample data is right' do
    expect(comment).to be_valid
  end

  it 'comment not should be empty' do
    comment.body = ''
    expect(comment).not_to be_valid
  end

  it 'content is up to 100' do
    comment.body = 'a' * 101
    expect(comment).not_to be_valid
  end
end
