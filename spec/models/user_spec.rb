require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:tom) }

  it 'sample data is right' do
    expect(user).to be_valid
  end

  it 'user_name should not be nil' do
    user.user_name = nil
    expect(user).not_to be_valid
  end

  it 'user_name should not be too long' do
    user.user_name = "a" * 31
    expect(user).not_to be_valid
  end

  it 'email should not be too long' do
    user.email = "a" * 244 + "@example.com"
    expect(user).not_to be_valid
  end

  it 'email validation should reject invalid addresses' do
    invalid_addresses = %w(
      user@example,com user_at_foo.org user.name@example.
      foo@bar_baz.com foo@bar+baz.com
    )
    invalid_addresses.each do |invalid_address|
      user.email = invalid_address
      expect(user).not_to be_valid
    end
  end

  it 'email address is unique' do
    user.email = User.create(email: 'foobar@email.com')
    user.email.upcase
    user.save
    expect(user).not_to be_valid
  end

  it 'email address not should be empty' do
    user.email = ''
    expect(user).not_to be_valid
  end

  it 'password should be present (nonblank)' do
    user.password = user.password_confirmation = "" * 6
    expect(user).not_to be_valid
  end

  it 'password should have a minimum length' do
    user.password = user.password_confirmation = "a" * 5
    expect(user).not_to be_valid
  end
end
