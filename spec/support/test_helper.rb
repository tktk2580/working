module TestHelper
  def sign_in_as(user)
    # user = User.create(name: "tom",nickname:"tom",email:"hogehoge1@email.com",password: "foobar")
    visit login_path
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    click_button 'Log in'
    expect(page).to have_content 'Signed in successfully.'
  end
end
