FactoryBot.define do
  factory :kirito, :class => "Comment" do
    content { "MyString" }
    user_id { 1 }
    micropost_id { 1 }
    body { 'コメント' }
    association :user, factory: :micher
    association :micropost, factory: :orange
  end
end
