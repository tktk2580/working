FactoryBot.define do
  factory :tom, class: :User do
    name  { 'tom brawn' }
    user_name { 'tom' }
    email { 'foobar@email.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    gender { '男' }
    comment { 'nice to meet you' }
    id { '1' }
  end
  factory :micher, class: :User do
    name  { 'michael wonder' }
    user_name { 'micher' }
    email { ' michael@example.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    gender { '男' }
    comment { 'hello everyone' }
    admin { 'true' }
  end
end
