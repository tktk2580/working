require 'rails_helper'

RSpec.feature 'Like', type: :feature do
  let(:user) { FactoryBot.create(:tom) }
  let(:comment_user) { FactoryBot.create(:micher) }
  let(:micropost) { FactoryBot.create(:orange, user_id: comment_user.id) }

  scenario 'click form button and cancel button' do
    sign_in_as(user)
    visit micropost_path(micropost.id)
    expect(page).to have_content "I just ate an orange!"
    click_button '教えてもらう'
    expect(current_path).to eq "/microposts/1"
    expect(page).to have_content "インストラクターに通知を送りました。"
    expect(page).to have_content "申し込み済！！"
    click_button 'キャンセルする'
    expect(current_path).to eq "/microposts/1"
    expect(page).to have_content "レッスンをキャンセルしました。"
    expect(page).not_to have_content "申し込み済！！"
  end
end
