require 'rails_helper'

RSpec.feature "UsersIndices", type: :feature do
  include TestHelper
  let!(:nonadmin) { FactoryBot.create(:tom) }
  let!(:admin) { FactoryBot.create(:micher) }

  scenario 'visit index page when not have admin' do
    sign_in_as(nonadmin)
    visit users_path
    expect(page).to have_content "インストラクターを探す"
    expect(page).to have_content admin.user_name
    expect(page).to have_selector 'a', text: 'delete', count: 0
  end

  scenario 'visit index page when have admin' do
    sign_in_as(admin)
    visit users_path
    expect(page).to have_content "インストラクターを探す"
    expect(page).to have_selector 'a', text: 'delete', count: 1
    click_link "delete"
    expect(page).to have_selector 'a', text: 'delete', count: 0
  end

  scenario 'search user' do
    sign_in_as(nonadmin)
    visit users_path
    expect(page).to have_content nonadmin.user_name
    expect(page).to have_content admin.user_name
    fill_in 'search', with: admin.user_name
    click_button "Search"
    expect(page).not_to have_content nonadmin.user_name
    expect(page).to have_content admin.user_name
  end

  scenario 'direct to user page when click user_nmae' do
    sign_in_as(nonadmin)
    visit users_path
    click_link "micher"
    expect(current_path).to eq "/users/2"
  end

  scenario 'visit index page when not login' do
    visit users_path
    expect(current_path).to eq login_path
  end
end
