require 'rails_helper'

RSpec.feature 'CommentsNew', type: :feature do
  let!(:user) { FactoryBot.create(:tom) }
  let!(:user1) { FactoryBot.create(:micher) }
  let(:micropost) { FactoryBot.create(:orange, user_id: user1.id) }

  scenario 'comment notification' do
    sign_in_as(user)
    visit micropost_path(micropost.id)
    expect(page).to have_content "I just ate an orange!"
    fill_in 'comment[body]', with: "nice to meet you"
    click_button 'コメント'
    expect(page).to have_content "コメントしました。"
    click_link 'マイページ'
    expect(current_path).to eq "/users/1"
    click_link 'ログアウト'
    expect(current_path).to eq root_path
    sign_in_as(user1)
    click_link '通知'
    expect(current_path).to eq notifications_path
    expect(page).to have_content "コメントしました。"
    click_link user.user_name
    expect(current_path).to eq "/users/1"
  end

  scenario 'form lesson notification' do
    sign_in_as(user)
    visit micropost_path(micropost.id)
    expect(current_path).to eq "/microposts/1"
    click_button '教えてもらう'
    expect(page).to have_content "インストラクターに通知を送りました。"
    click_link 'マイページ'
    expect(current_path).to eq "/users/1"
    click_link 'ログアウト'
    expect(current_path).to eq root_path
    sign_in_as(user1)
    click_link '通知'
    expect(current_path).to eq notifications_path
    expect(page).to have_content "申し込みました。"
    click_link user.user_name
    expect(current_path).to eq "/users/1"
  end

  scenario 'follow notification' do
    sign_in_as(user)
    visit users_path
    click_link user1.user_name
    expect(current_path).to eq "/users/2"
    click_button 'Follow'
    click_link 'マイページ'
    expect(current_path).to eq "/users/1"
    click_link 'ログアウト'
    expect(current_path).to eq root_path
    sign_in_as(user1)
    click_link '通知'
    expect(current_path).to eq notifications_path
    expect(page).to have_content "あなたをフォローしました。"
    click_link user.user_name
    expect(current_path).to eq "/users/1"
  end
end
