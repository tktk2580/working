require 'rails_helper'

RSpec.feature 'Follow', type: :feature do
  let!(:follow_user) { FactoryBot.create(:tom) }
  let!(:followed_user) { FactoryBot.create(:micher) }

  scenario 'follow,follower_user is displayed correctly when follow' do
    sign_in_as(follow_user)
    visit users_path
    click_link followed_user.user_name
    expect(current_path).to eq "/users/2"
    click_button 'Follow'
    click_link 'マイページ'
    expect(current_path).to eq "/users/1"
    click_link 'following'
    expect(current_path).to eq following_user_path(follow_user.id)
    expect(page).to have_content followed_user.user_name
    click_link 'マイページ'
    expect(current_path).to eq "/users/1"
    click_link 'ログアウト'
    expect(current_path).to eq root_path
    sign_in_as(followed_user)
    click_link 'マイページ'
    expect(current_path).to eq "/users/2"
    click_link 'followers'
    expect(current_path).to eq followers_user_path(followed_user.id)
    expect(page).to have_content follow_user.user_name
  end
end
