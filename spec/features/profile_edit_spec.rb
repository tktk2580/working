require 'rails_helper'

RSpec.feature "ProfileEdits", type: :feature do
  include TestHelper
  let(:user) { FactoryBot.create(:tom) }

  scenario 'comment edit is successfly' do
    sign_in_as(user)
    visit users_path(1)
    expect(page).to have_content 'nice to meet you'
    expect(page).not_to have_content 'helloworld'
    visit profile_edit_path
    fill_in 'user[user_name]', with: user.user_name
    fill_in 'user[name]', with: user.name
    fill_in 'user[email]', with: user.email
    select '男', from: 'user[gender]'
    fill_in 'user[comment]', with: 'helloworld'
    click_button '更新'
    expect(page).to have_content 'プロフィールを更新しました'
    expect(current_path).to eq "/users/1"
    expect(page).not_to have_content 'nice to meet you'
    expect(page).to have_content 'helloworld'
  end

  scenario 'email shold not be empty' do
    sign_in_as(user)
    visit profile_edit_path
    fill_in 'user[user_name]', with: user.user_name
    fill_in 'user[name]', with: user.name
    fill_in 'user[email]', with: ''
    select '男', from: 'user[gender]'
    fill_in 'user[comment]', with: user.comment
    click_button '更新'
    expect(current_path).to eq profile_update_path
  end

  scenario 'delete account' do
    sign_in_as(user)
    visit profile_edit_path
    click_button '削除'
    expect(current_path).to eq root_path
    expect(page).to have_content 'Bye! Your account has been successfully cancelled.
                        We hope to see you again soon.'
  end
end
