require 'rails_helper'

RSpec.feature "home", type: :feature do
  let(:user) { FactoryBot.create(:tom) }
  let(:micropost) { FactoryBot.create(:orange) }

  before do
    visit root_path
  end

  scenario "page is displayed correctly" do
    expect(page).to have_content "最新のレッスン"
  end

  scenario "click header home" do
    click_link "Home"
    expect(current_path).to eq root_path
  end

  scenario "click header logo" do
    click_link "soonikl"
    expect(current_path).to eq root_path
  end

  scenario "click header signup" do
    click_link "新規登録"
    expect(current_path).to eq new_user_registration_path
  end

  scenario "click header login" do
    click_link "ログイン"
    expect(current_path).to eq login_path
  end

  scenario "click header search instructor" do
    click_link "インストラクターを探す"
    expect(current_path).to eq login_path
  end

  scenario "click header search lesson" do
    click_link "レッスンを探す"
    expect(current_path).to eq search_path
  end

  scenario "click level1" do
    click_link "初級者"
    expect(current_path).to eq level1_path
  end

  scenario "click level2" do
    click_link "中級者"
    expect(current_path).to eq level2_path
  end

  scenario "click level3" do
    click_link "上級者"
    expect(current_path).to eq level3_path
  end
end
