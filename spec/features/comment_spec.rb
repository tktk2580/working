require 'rails_helper'

RSpec.feature 'CommentsNew', type: :feature do
  let(:user) { FactoryBot.create(:tom) }
  let(:comment_user) { FactoryBot.create(:micher) }
  let(:micropost) { FactoryBot.create(:orange, user_id: comment_user.id) }

  scenario 'comment correctly and delete comment' do
    expect do
      sign_in_as(user)
      visit micropost_path(micropost.id)
      expect(page).to have_content "I just ate an orange!"
      fill_in 'comment[body]', with: "nice to meet you"
      click_button 'コメント'
      expect(current_path).to eq "/microposts/1"
      expect(page).to have_content "コメントしました。"
      expect(page).to have_content "nice to meet you"
    end.to change(Comment, :count). by(1)
    click_link '削除'
    expect(current_path).to eq "/microposts/1"
    expect(page).to have_content "コメントを削除しました。"
    expect(page).not_to have_content "nice to meet you"
  end

  scenario 'comment box is empty' do
    expect do
      sign_in_as(user)
      visit micropost_path(micropost.id)
      expect(page).to have_content "I just ate an orange!"
      fill_in 'comment[body]', with: ""
      click_button 'コメント'
      expect(current_path).to eq "/microposts/1"
      expect(page).to have_content "コメント欄に何も記入されていません。"
    end.to change(Comment, :count). by(0)
  end

  scenario 'content is up to 100' do
    expect do
      sign_in_as(user)
      visit micropost_path(micropost.id)
      expect(page).to have_content "I just ate an orange!"
      fill_in 'comment[body]', with: 'a' * 101
      click_button 'コメント'
      expect(current_path).to eq "/microposts/1"
      expect(page).to have_content "100字以内でコメントしてください。"
    end.to change(Comment, :count). by(0)
  end
end
