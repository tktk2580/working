require 'rails_helper'

RSpec.feature "PasswordEdits", type: :feature do
  include TestHelper
  let(:user) { FactoryBot.create(:tom) }

  scenario 'password chage is successfly' do
    visit login_path
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: 'foobar'
    click_button 'Log in'
    expect(current_path).to eq root_path
    visit edit_user_registration_path(user.id)
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: 'password'
    fill_in 'user[password_confirmation]', with: 'password'
    fill_in 'user[current_password]', with: user.password
    click_button '更新'
    expect(current_path).to eq root_path
    expect(page).to have_content "Your account has been updated successfully."
    click_link 'マイページ'
    expect(current_path).to eq "/users/1"
    click_link 'ログアウト'
    expect(current_path).to eq root_path
    click_link 'ログイン'
    expect(current_path).to eq login_path
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: 'password'
    click_button 'Log in'
    expect(current_path).to eq root_path
    expect(page).to have_content "Signed in successfully."
  end

  scenario 'misstaking current passward should not chage passward' do
    sign_in_as(user)
    visit edit_user_registration_path(user.id)
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: 'password'
    fill_in 'user[password_confirmation]', with: 'password'
    fill_in 'user[current_password]', with: 'wrong-password'
    click_button '更新'
    expect(current_path).to eq users_path
    expect(page).to have_content "Current password is invalid"
  end
end
