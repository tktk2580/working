require 'rails_helper'

RSpec.feature 'UserSignup', type: :feature do
  let(:user) { FactoryBot.create(:tom) }

  scenario 'login is successfly to logout' do
    visit login_path
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    click_button 'Log in'
    expect(current_path).to eq root_path
    expect(page).to have_content "Signed in successfully."
    click_link "マイページ"
    expect(current_path).to eq "/users/1"
    click_link "ログアウト"
    expect(page).to have_content "Signed out successfully."
    expect(current_path).to eq root_path
  end

  scenario 'address is not inputted' do
    visit login_path
    fill_in 'user[email]', with: ''
    fill_in 'user[password]', with: user.password
    click_button 'Log in'
    expect(current_path).to eq "/users/sign_in"
    expect(page).to have_content "Invalid Email or password."
  end

  scenario 'address and password is wrong' do
    visit login_path
    fill_in 'user[email]', with: 'abcd@example.com'
    fill_in 'user[password]', with: 'abcdefg'
    click_button 'Log in'
    expect(current_path).to eq "/users/sign_in"
    expect(page).to have_content "Invalid Email or password."
  end
end
