require 'rails_helper'

RSpec.feature 'MicropostsNew', type: :feature do
  let!(:user) { FactoryBot.create(:tom) }

  scenario 'making new lesson is successfly and delete lesson' do
    @micropost = Micropost.new(content: "プログラミングを教えます", level: "初級者",
                               picture: Rack::Test::UploadedFile.
                               new(File.join(Rails.root, 'app/assets/images/rails.png')),
                               price: " 1000円/60分", service: "基礎から教えます。", other: "丁寧に教えます。",
                               category: "デザイン")
    expect do
      expect do
        sign_in_as(user)
        visit new_micropost_path
        fill_in 'micropost[content]', with: @micropost.content
        select @micropost.level, from: 'micropost[level]'
        attach_file 'micropost[picture]', 'app/assets/images/rails.png'
        fill_in 'micropost[price]', with: @micropost.price
        fill_in 'micropost[service]', with: @micropost.service
        fill_in 'micropost[other]', with: @micropost.other
        select @micropost.category, from: 'micropost[category]'
        click_button '登録'
        expect(current_path).to eq "/microposts/1"
        expect(page).to have_content "新しいLessonを開講しました！！"
      end.to change(Micropost, :count). by(1)
      visit micropost_path(1)
      expect(page).to have_content "プログラミングを教えます"
      click_link 'このLessonを削除する'
      expect(current_path).to eq root_path
      expect(page).to have_content "Lessonを削除しました。"
    end.to change(Micropost, :count). by(0)
  end

  scenario 'errors are displayed correctly' do
    @micropost = Micropost.new(content: "プログラミングを教えます", level: "初級者",
                               picture: Rack::Test::UploadedFile.
                               new(File.join(Rails.root, 'app/assets/images/rails.png')),
                               price: " 1000円/60分", service: "基礎から教えます。", other: "丁寧に教えます。",
                               category: "デザイン")
    expect do
      sign_in_as(user)
      visit new_micropost_path
      fill_in 'micropost[content]', with: ""
      select @micropost.level, from: 'micropost[level]'
      attach_file 'micropost[picture]', 'app/assets/images/rails.png'
      fill_in 'micropost[price]', with: ""
      fill_in 'micropost[service]', with: ""
      click_button '登録'
      expect(current_path).to eq "/microposts"
      expect(page).to have_content "Content can't be blank"
      expect(page).to have_content "Price can't be blank"
      expect(page).to have_content "Service can't be blank"
    end.to change(Micropost, :count). by(0)
  end
end
