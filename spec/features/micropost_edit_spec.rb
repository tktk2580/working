require 'rails_helper'

RSpec.feature 'MicropostEdit', type: :feature do
  let(:user) { FactoryBot.create(:tom) }
  let(:micropost) { FactoryBot.create(:orange, user_id: user.id) }

  scenario 'edit lesson' do
    @micropost = Micropost.new(content: "プログラミングを教えます", level: "初級者",
                               picture: Rack::Test::UploadedFile.
                               new(File.join(Rails.root, 'app/assets/images/rails.png')),
                               price: " 1000円/60分", service: "基礎から教えます。", other: "丁寧に教えます。",
                               category: "デザイン")
    sign_in_as(user)
    visit micropost_path(micropost.id)
    expect(page).to have_content "I just ate an orange!"
    click_link 'このLessonを編集する'
    expect(page).to have_content "レッスンの編集"
    fill_in 'micropost[content]', with: @micropost.content
    select @micropost.level, from: 'micropost[level]'
    attach_file 'micropost[picture]', 'app/assets/images/rails.png'
    fill_in 'micropost[price]', with: @micropost.price
    fill_in 'micropost[service]', with: @micropost.service
    fill_in 'micropost[other]', with: @micropost.other
    select @micropost.category, from: 'micropost[category]'
    click_button '登録'
    expect(current_path).to eq micropost_path(micropost.id)
    expect(page).to have_content "編集成功！！"
  end

  scenario 'errors are displayed correctly' do
    @micropost = Micropost.new(content: "プログラミングを教えます", level: "初級者",
                               picture: Rack::Test::UploadedFile.
                               new(File.join(Rails.root, 'app/assets/images/rails.png')),
                               price: " 1000円/60分", service: "基礎から教えます。", other: "丁寧に教えます。",
                               category: "デザイン")
    sign_in_as(user)
    visit edit_micropost_path(micropost.id)
    expect(page).to have_content "レッスンの編集"
    fill_in 'micropost[content]', with: ""
    select @micropost.level, from: 'micropost[level]'
    attach_file 'micropost[picture]', 'app/assets/images/rails.png'
    fill_in 'micropost[price]', with: ""
    fill_in 'micropost[service]', with: ""
    click_button '登録'
    expect(current_path).to eq "/microposts/1"
    expect(page).to have_content "Content can't be blank"
    expect(page).to have_content "Price can't be blank"
    expect(page).to have_content "Service can't be blank"
  end
end
