require 'rails_helper'

RSpec.describe "HomeController", type: :request do
  describe 'GET #index' do
    let(:base_title) { 'Working' }

    it "responds successfully" do
      get root_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #search' do
    let(:base_title) { 'Search | Working' }

    it "responds successfully" do
      get search_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end
end
