require 'rails_helper'

RSpec.describe 'Microposts', type: :request do
  include TestHelper

  let(:user) { FactoryBot.build(:tom) }
  let(:user2) { FactoryBot.build(:micher) }
  let(:micropost) { FactoryBot.build(:orange) }

  describe 'GET /microposts' do
    context 'ユーザーを消した時' do
      it 'マイクロポストも一緒に消される' do
        expect do
          user.microposts.build!(content: 'Lorem ipusum')
          user.destroy
        end
        expect(Micropost.where(id: 1).count).to eq 0
      end
    end
  end
end
