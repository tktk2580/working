require 'rails_helper'

RSpec.describe "UsersController", type: :request do
  describe 'GET #sign_up' do
    let(:base_title) { 'Working' }

    it 'gets sign_up' do
      get new_user_registration_path
      expect(response).to be_successful
      assert_select "title", "sign-up | #{base_title}"
    end
  end

  describe 'GET #sign_in' do
    let(:base_title) { 'Working' }

    it 'gets sign_up' do
      get new_user_session_path
      expect(response).to be_successful
      assert_select "title", "log-in | #{base_title}"
    end
  end
end
