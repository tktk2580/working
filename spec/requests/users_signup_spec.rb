require 'rails_helper'

RSpec.describe 'UsersSignups', type: :request do
  describe 'GET /users_signups' do
    context 'valid signup information' do
      it 'valid signup information' do
        get signup_path
        expect do
          post signup_path, params: { user: {
            user_name: 'ExampleUser',
            email: 'user@example.com',
            password: '123123',
            password_confirmation: '123123',
          } }
        end.
          to change(User, :count).by(1)
      end
    end

    context 'invalid signup information' do
      before do
        create(:tom, email: 'user1@example.com')
      end

      it 'user_name shold not be empty' do
        get signup_path
        expect do
          post signup_path, params: { user: {
            user_name: '',
            email: 'user@example.com',
            password: '123123',
            password_confirmation: '123123',
          } }
        end.
          to change(User, :count).by(0)
      end

      it 'email shold not be empty' do
        get signup_path
        expect do
          post signup_path, params: { user: {
            user_name: 'ExampleUser',
            email: '',
            password: '123123',
            password_confirmation: '123123',
          } }
        end.
          to change(User, :count).by(0)
      end

      it 'password shold not be empty' do
        get signup_path
        expect do
          post signup_path, params: { user: {
            user_name: 'ExampleUser',
            email: 'user@example.com',
            password: '',
            password_confirmation: '123123',
          } }
        end.
          to change(User, :count).by(0)
      end

      it 'invalid arlredy email' do
        get signup_path
        expect do
          post signup_path, params: { user: {
            user_name: 'ExampleUser',
            email: 'user1@example.com',
            password: '123123',
            password_confirmation: '123123',
          } }
        end.
          to change(User, :count).by(0)
      end
    end
  end
end
