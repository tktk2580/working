require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe 'GET #design' do
    let(:base_title) { 'デザイン | Working' }

    it "responds successfully" do
      get design_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #music' do
    let(:base_title) { '音楽 | Working' }

    it "responds successfully" do
      get music_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #movie' do
    let(:base_title) { '動画、写真、画像 | Working' }

    it "responds successfully" do
      get movie_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #it' do
    let(:base_title) { 'IT、プログラミング | Working' }

    it "responds successfully" do
      get it_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #business' do
    let(:base_title) { 'ビジネス | Working' }

    it "responds successfully" do
      get business_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #language' do
    let(:base_title) { '語学 | Working' }

    it "responds successfully" do
      get language_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #beauty' do
    let(:base_title) { '美容 | Working' }

    it "responds successfully" do
      get beauty_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #hobby' do
    let(:base_title) { '趣味、エンタメ | Working' }

    it "responds successfully" do
      get hobby_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #other' do
    let(:base_title) { 'その他 | Working' }

    it "responds successfully" do
      get other_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #level1' do
    let(:base_title) { '初級者 | Working' }

    it "responds successfully" do
      get level1_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #level2' do
    let(:base_title) { '中級者 | Working' }

    it "responds successfully" do
      get level2_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end

  describe 'GET #level3' do
    let(:base_title) { '上級者 | Working' }

    it "responds successfully" do
      get level3_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match(/<title>#{base_title}<\/title>/i)
    end
  end
end
